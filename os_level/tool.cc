#include <iostream>
#include <string>
using namespace std;

int teller(string zin, string karakter){
	int count = 0;
	if(karakter.size() == 1){
	char karakterchar = karakter[0];
	for(int i = 0; i < zin.size(); i++){
		if(karakterchar == zin[i]){
			count += 1;
			}
		}
	}
	return count;
}

int main(int argc, char *argv[]){
string zin = "Linux test opdracht nul";
	if(argc != 2)
  	{cout << "Deze functie heeft exact 1 argument nodig" << endl;
    	return -1;

	}else{
  		cout << teller(zin, argv[1]) << endl; 
  		return 0;}
}
