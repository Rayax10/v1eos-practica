#!/usr/bin/env bash

echo "Naam: "
read naam;
imposter=$(whoami)
if [ -z "$naam" ]; 
then
    naam=$imposter
    echo "Welkom $naam"
fi


while [ true ] ; do
    echo "Wachtwoord: " 
    read wachtwoord;
    tmp=$wachtwoord

    if [ ${#tmp} -ge 8 ] ; then
        echo "Wachtwoord: "
        read wachtwoord1;
    else 
        echo "Wachtwoord is kleiner dan 8 karakters"
        break
    fi
    if [ $wachtwoord1 = $tmp ]; then
        echo "Wachtwoord correct!"
        break

    else 
        echo "Wachtwoord incorrect!"
    fi   
done

    md5=$( echo $wachtwoord1 | md5sum | awk '{print $wachtwoord1}')
    echo $md5
    file="$naam, $md5"
    echo $file >> maakLogin_passwordfile


