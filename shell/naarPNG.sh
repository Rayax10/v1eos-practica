#!/usr/bin/env bash

lva=$(find -type f -name *.jpg) #type file file name .jpg
for foto in $lva; do
    IFS='./' 
    read -a df <<< "$foto" #deelfoto
    eval convert ${df[2]}.${df[3]} -resize 16384@ ${df[2]}.png
done