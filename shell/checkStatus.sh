#!/usr/bin/env bash

name=$2
> $name
for file in $(find .); do
        if $1 "$file" >/dev/null 2>&1; then
                echo "$file = gelukt!" >> $name
        else
                echo "$file = mislukt!" >> $name
        fi
done
cat $2