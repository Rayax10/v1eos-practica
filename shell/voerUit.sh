#!/usr/bin/env bash
IFS='.'
read -a stringarray <<< "$1"
case ${stringarray[1]} in
    py) eval python3 ${stringarray[0]}.${stringarray[1]}
    ;;
    cc) eval cat ${stringarray[0]}.${stringarray[1]}
    ;;
    sh) eval bash ${stringarray[0]}.${stringarray[1]}
    ;;
    *) echo "Error"
;;
esac